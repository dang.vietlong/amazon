const i18n = require('eleventy-plugin-i18n')
const translations = require('./src/data/i18n')

module.exports = function (eleventyConfig) {
  eleventyConfig.addPassthroughCopy('assets')

  eleventyConfig.addNunjucksFilter('makeDashCase', function (value) {
    return value.trim().split(' ').join('-')
  })

  eleventyConfig.addPlugin(i18n, {
    translations,
    fallbackLocales: {
      '*': 'en'
    }
  })

  return {
    addPassthroughFileCopy: true,
    markdownTemplateEngine: 'njk',
    templateFormats: ['html', 'njk', 'md'],
    dir: {
      input: 'src',
      output: '_site',
      includes: 'includes',
      layouts: 'layouts',
      data: 'data',
    },
  }
}
