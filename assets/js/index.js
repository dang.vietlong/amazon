(function () {
  const articleMenu = $('.menu__list')
  const articleBurgerMenu = $('.menu__burger-btn')
  const allProductsCarousel = $('#all-products__slider')
  const allProductsPagination = $('#all-products-pagination')
  const productList = $('.product-info')
  const mediaScreenSm = 700
  const mediaScreenMd = 1020
  const mediaScreenLg = 1300
  let slidesToShow = 4

  if (window.innerWidth < mediaScreenSm) {
    slidesToShow = 1
  } else if (window.innerWidth < mediaScreenMd) {
    slidesToShow = 2
  } else if (window.innerWidth < mediaScreenLg) {
    slidesToShow = 3
  }

  articleBurgerMenu.on('click', function () {
    articleMenu.toggleClass('list-display-none')
  })

  productList.map((index, elem) => {
    const productCarousel = elem.querySelector('.pictures-list')
    const nextArrow = elem.querySelector('.right-slide')
    const prevArrow = elem.querySelector('.left-slide')
    const productPagination = elem.querySelector('.item-number__current')

    $(productCarousel).slick({
      prevArrow, nextArrow,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            arrows: false,
          },
        },
      ],
    })

    $(productCarousel).on('afterChange', (event, slick, currentSlide) =>
      productPagination.textContent = (currentSlide ? currentSlide : 0) + 1
    )
  })

  const renderAllProductsPagination = (currentSlide = 1, totalCount) =>
    allProductsPagination.html(`<b class="item-number__current">${currentSlide}</b>/${Math.ceil(totalCount)}`)

  renderAllProductsPagination(1, ALL_PRODUCTS_COUNT / slidesToShow)

  allProductsCarousel.slick({
    slidesToScroll: 4,
    variableWidth: true,
    draggable: false,
    infinite: false,
    prevArrow: $('#all-products-slide-prev'),
    nextArrow: $('#all-products-slide-next'),
    responsive: [
      {
        breakpoint: mediaScreenLg,
        settings: {
          slidesToScroll: 3
        }
      },
      {
        breakpoint: mediaScreenMd,
        settings: {
          slidesToScroll: 2
        }
      },
      {
        breakpoint: mediaScreenSm,
        settings: {
          slidesToScroll: 1
        }
      }
    ]
  })

  allProductsCarousel.on('afterChange', function (event, slick, currentSlide) {
    const slide = Math.ceil(currentSlide / slidesToShow) + 1
    renderAllProductsPagination(slide, ALL_PRODUCTS_COUNT / slidesToShow)
  })
}())
